<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditProductRequest;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use App\Product;
use App\Repositories\ProductRepository;
use App\Services\IndexService;
use Illuminate\Support\Str;

class ProductController extends Controller
{

     public function __construct()
     {
          $this->mainService = new IndexService;
          $this->ProductRepo = new ProductRepository;
     }

     /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function index(request $request)
     {
          $key = (empty($request->key) ? '' : str_replace($this->mainService->fullWidth(), $this->mainService->halfWidth(), $request->key));
          $pg = (empty($request->pg) ? 1 : $pg = $request->pg);
          $view = (empty($request->view) ? 10 : $view = $request->view);
          $sort = (empty($request->desc) ? 'desc' : $sort = $request->desc);
          $orderby = (empty($request->orderby) ? 'created_at' : $orderby = $request->orderby);
          $num = (($pg - 1) * $view);
          $new_s = htmlspecialchars($key, ENT_QUOTES); //轉換單引號與雙引號
          $product = $this->mainService->mainList($new_s, $key, $orderby, $sort);

          foreach ($product as $info) {
               $views = $info['list']->skip($num)->take($view)->get();
               $count = $info['count'];
          }

          return View('backend.product.index', [
               'no' => 1,
               'title' => '商品管理平台-首頁',
               'now' => 'product',
               'count' => $count,
               'list' => $views,
               'sort' => $sort,
               'key' => $key,
               'orderby' => $orderby,
               'pg' => $pg,
               'view' => $view,
               'num' => $num,
               'search' => '',
          ]);
     }

     public function create()
     {
          return View('backend.product.create', [
               'no' => 1,
               'key' => '此分頁沒有查詢功能',
               'now' => 'product',
               'title' => '商品管理平台-新增資料',
               'search' => 'disabled="disabled"',
          ]);
     }

     public function add(ProductRequest $request)
     {
          $id = $this->ProductRepo->insertData($request);
          if (!empty($id)) {
               switch ($request->input('method')) {
                    case 0:
                         return redirect('/backend/create');
                         break;
                    case 1:
                         return redirect('/backend');
                         break;
                    case 2:
                         return redirect('/backend/edit/' . $id);
                         break;
               }
          }
     }

     public function edit($id)
     {
          return View('backend.product.edit', [
               'no' => 1,
               'key' => '此分頁沒有查詢功能',
               'now' => 'product',
               'title' => '商品管理平台-編輯資料',
               'search' => 'disabled="disabled"',
               'product' => $this->ProductRepo->editDataGet($id),
          ]);
     }

     public function update(EditProductRequest $request, $id)
     {
          $this->ProductRepo->editData($request, $id);
          switch ($request->input('method')) {
               case 0:
                    return redirect('/backend/create');
                    break;
               case 1:
                    return redirect('/backend');
                    break;
               case 2:
                    return redirect('/backend/edit/' . $id);
                    break;
          }
     }

     public function delete(Request $request)
     {
          $product = Product::find($request->id);
          $product->delete();
     }

     public function frontend()
     {

          $views = Product::orderBy('created_at', 'desc')->get();

          return View('frontend.index', [
               'product' => $views,
          ]);
     }
}
