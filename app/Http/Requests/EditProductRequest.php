<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->url == '' and $this->picture == '') {
            return [
                'Name' => 'required|', //.Rule::unique('glory','gloryname')->where('deleted_at','NULL'),
                'o_price' => 'required',
                'price' => 'required',
                'url' => 'nullable|url',
                'cover7' => 'between:0,1024|mimes:jpeg,jpg,png,gif' //限定上傳大小及格式
            ];
        } else if ($this->url != '') {
            return [
                'Name' => 'required|', //.Rule::unique('glory','gloryname')->where('deleted_at','NULL'),
                'o_price' => 'numeric|required',
                'price' => 'nullable|numeric',
                'url' => 'url',
                'cover7' => 'between:0,1024|mimes:jpeg,jpg,png,gif' //限定上傳大小及格式
            ];
        }
    }
}
