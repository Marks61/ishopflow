<?php

namespace App\Repositories;

use App\Product;

class ProductRepository
{
    public function __construct()
    {
        $this->product = new Product();
    }

    public function mainList($new_s, $key, $orderby, $sort)
    {
        $index = $this->createIndex($new_s, $key, $orderby, $sort);

        return array([
            'list' => $index->orderby($orderby, $sort),
            'count' => count($index->get())
        ]);
    }

    public function createIndex($new_s, $key)
    {
        return $this->product::whereRaw('cast(created_at as char) like "%' . $new_s . '%"')
            ->orwhere('name', 'like', '%' . $key . '%');
    }

    public function insertData($request)
    {
        $product = $this->product;

        $path = public_path() . '/images/product/'; //定義檔案上傳位址

        $upload_file = $request->file('cover7');

        if ($request->p_url == '') {
            if ($request->hasfile('cover7') && $upload_file->isValid()) {
                $newname = 'img' . uniqid() . '.' . $request->file('cover7')->getClientOriginalExtension(); //重新命名

                $upload_file->move($path, $newname); //上傳檔案

                $product->picture = 'public/images/product/' . $newname;
            }
        } else {
            $product->picture = $request->p_url;
        }

        $product->name = $request->Name;
        $product->price = $request->o_price;
        $product->sPrice = $request->price;
        $product->save();

        return $product->id;
    }

    public function editDataGet($id)
    {
        return $this->product::find($id);
    }

    public function editData($request, $id)
    {
        $product = $this->editDataGet($id);

        $path = public_path() . '/images/product/'; //定義檔案上傳位址

        $upload_file = $request->file('cover7');

        if ($request->url == '' and $product->picture == '') {
            if ($request->hasfile('cover7') && $upload_file->isValid()) {
                $newname = 'img' . uniqid() . '.' . $request->file('cover7')->getClientOriginalExtension(); //重新命名

                $upload_file->move($path, $newname); //上傳檔案

                $product->picture = 'public/images/product/' . $newname;
            }
        } else if ($request->url != '') {
            $product->picture = $request->url;
        } else {
        }

        $product->name = $request->Name;
        $product->price = $request->o_price;
        $product->sPrice = $request->price;
        $product->touch();

        return true;
    }
}
