<!DOCTYPE html>
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{asset('/backstage/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{asset('/backstage/js/jquery-3.3.1.js')}}"></script>
        <style>
            @charset "UTF-8";
            @import url(https://fonts.googleapis.com/earlyaccess/cwtexyen.css);
            @import url(https://fonts.googleapis.com/earlyaccess/notosanstc.css);
        </style>
    </head>
    <body>
        <header class="navbar navbar-expand-lg navbar-dark" style="border-top: 3px solid orange;background-color:#999998;">
            <div class="col-md-12 h2 text-muted" >猜你喜歡</div>
        </header>
        <div class="row container-fluid" style="margin-top: 2%; " id="container">
            @foreach($product as $row)
               <div class="panel col-md-4 col-sm-6 article">
                    <div class="panel panel-heading" style="heigh:600px;">
                        <img class="w-75" src="{{asset($row->picture)}}" style="heigh:650px;" />
                    </div>
                    <div class="panel panel-body h6 text-muted">
                        {{$row->name}}
                    </div>
                    <div class="panel panel-footer row">
                        <h6 class="col-md-6 col-lg-6 col-xs-6 text-left text-muted">${{$row->price}}</h6>
                        <h6 class="col-md-6 col-lg-6 col-xs-6 text-left text-danger">${{$row->sPrice}}</h6>
                    </div>
                </table>
            </div> 
            @endforeach
        </div>
    </body>
</html>
